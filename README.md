# kyanne.gitlab.com

Kyanne's personal website.

## To do

- [X] Add RubyGems
- [X] Add categories, tags
- [X] Add default variables to config.yml
- [X] Configure contact form
- [ ] **Add subscription form to footer**
- [X] Replace Font Awesome 4 with Font Awesome 5
- [X] Create favicon
- [ ] **Clean up SCSS/CSS**
- [X] Formalize color palette
- [X] Add resume
- [X] Redesign Blog page
- [ ] **Redesign post layout**
- [ ] **Create Projects page**

## Credits

Made using the Spectral theme, designed by HTML5 UP and integrated into Jekyll by [Andrew Banchich](https://gitlab.com/andrewbanchich).

[Source code](https://gitlab.com/andrewbanchich/spectral-jekyll-theme)

[Demo](https://andrewbanchich.gitlab.io/spectral-jekyll-theme/)

Images: U.S. Army Photo
