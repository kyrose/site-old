---
layout: post
title: What makes Git so powerful?
timestamp: 25 Nov 2018
excerpt: "Git is a free and open source Software Configuration Management tool. In this post I break down what the most commonly used descriptors for Git mean."
category: [blog]
tags: [git]
---

I’ve spent the last few months getting familiar with Python and though most of what I’ve written so far are mostly answers to course exercises and no programs worth sharing, I felt it was high time to get familiar with [Git](https://git-scm.com/). In this, my first post on Git, I break down what the most commonly used descriptors for Git mean.

Git is a [free and open source](http://freeopensourcesoftware.org/index.php/Main_Page) Software Configuration Management (SCM) tool, and more specifically a [distributed version control system](https://en.wikipedia.org/wiki/Distributed_version_control). Mainly used by software developers to manage changes in source code, Git was built to be versatile and can keep track of changes made in _any_ sort of file–meaning I can create and maintain a repository of PDFs if I were so inclined (I am).

But what exactly do all those adjectives and acronyms mean when it comes to the program’s functionality?

## Free and open source software

The concept of free and open source software (FOSS) is deserving of its own blog post, but in short: free (libre) software is released under a license that permits users to change and share it, and open source software has its source code available for anyone to review and improve. Typically, users are encouraged to modify FOSS, as the open collaboration tends to make for more secure, more private, more customizable software.

To read more about the principles and benefits of FOSS, check out the GNU General Public License v2 or GNU’s definition of free software.

## Software configuration management

Software configuration management (SMC) is the [job](https://books.google.com/books?id=CScMNjwwK8UC&lpg=PP1&dq=configuration%20management%20systems&pg=PP1#v=onepage&q&f=true) of identifying, regulating, reporting the status of, and auditing any changes made to software. The purpose of taking such measures to updating the source code of software is to ensure changes do not adversely affect the system, nor is the trust of the previous version compromised, ensuring that software remains secure.

Git isn’t strictly SCM, but it is [the most widely used](https://www.openhub.net/repositories/compare) system for SCM by software engineers as of November 2018, and hosts projects like the [Linux kernel](https://github.com/torvalds/linux), [Tor](https://gitweb.torproject.org/tor.git?a=tree;hb=HEAD), [Bootstrap](https://github.com/twbs/bootstrap), and The Freedom of the Press Foundation’s whistleblowing platform, [SecureDrop](https://github.com/freedomofpress/securedrop).

## Distributed version control

A version control system is a component of SCM, one which records every change made to a file or directory of files so that specific revisions may be recalled later. This is vital for software devs, web designers, and other programmers so that if a change is made that breaks the functionality of the program, you can revert to a previous version that worked. Files lost or corrupted can be easily recovered, too. For collaborative projects, version control allows those working together to see who changed what.

The ‘distributed’ attribute of this refers how the entire repository is mirrored on every computer that clones the official repository, contrasted with a centralized version control system, where the full history of a repository is stored on a single server and developers who clone the repository to work on it only grab the most recent version of it. Both types allow for collaboration on projects, but a distributed system can easily handle working with several different remote repositories for the same project. That allows a single dev to collaborate with different people on different aspects of the same project! But more important than that even, a distributed system doesn’t face the threat of a single point of failure leading to complete loss of the entire project–there’s many clones of the repository and its full history saved locally on people’s hard drives.

This distributed aspect of Git has another benefit, namely, speed. Since all operations are done locally Git performs far faster than those centralized systems that have to communicate with their servers. This is enormously beneficial for those working with huge repositories.

## So _that’s_ why people use Git…

All of the above characteristics of Git have contributed to its wide-scale adoption: it’s a community driven service known to be secure thanks to its source code being available for review, it’s a handy tool for ensuring that any updates to a project are done methodically to avoid adverse effects, and it outperforms other systems.

Join me for my next post on Git, where I try to explain one of the coolest features Git has to offer: branching.
