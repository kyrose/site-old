---
layout: post
category: [blog]

title: Hello World (again)!
timestamp: 17 May 2019  # ex: 10 Jun 2019
excerpt:  "Back in action!" # two to three sentence summary
image:
tags: [personal]  # in brackets
---

# Hello World (again)!

Welcome to my new website and blog! I decided to port everything over to GitLab Pages after I decided it wasn’t worth wrestling with Wordpress every time I wanted to add some customization to my pages. It took me longer than I intended to make the move not only because other responsibilities got in the way, but because I’m not just one to use a template without configuring it exactly to my specifications*. So, I had to learn SCSS/CSS on the fly. It was fun!

I’m going to return to making my classical encoder/decoder and blogging about that, but since I’ve caught a web dev bug, I’ll also post about what I learn as I add more functionality to this site and eventually create my own layout (I already have one in mind). Hope you enjoy those posts as much as I have been enjoying learning CSS.

My next post will be on adding a subscribe form to a static site like this one so you can keep track of my progress via e-mail!

*this site is a tweaked version of the [Spectral Jekyll Template](https://gitlab.com/andrewbanchich/spectral-jekyll-theme)
