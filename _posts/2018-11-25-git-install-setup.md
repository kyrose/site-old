---
layout: post
title: Installing and setting up Git
timestamp: 25 Nov 2018
excerpt: "How to install and run Git using the command line."
category: [blog]
tags: [git]
---

I run [Ubuntu 18.04 LTS](http://releases.ubuntu.com/18.04.1/) and use Git in the command line, so all examples included here will be for that environment. When I have time I’ll be sure to link to CLI instructions for Mac and Windows users.

If you’re unfamiliar with the command line the community run Git homepage has a list of GUIs for each OS [here](https://git-scm.com/downloads/guis/).

To install Git run:

```bash
$ sudo apt-get install git -y
```

Introduce yourself to Git with your name and e-mail:

```bash
$ git config --global user.name "Your name"
$ git config --global user.email user@example.com
```

Next, move into the directory your project files are saved in and create a new Git repository:

```bash
$ cd project
$ git init
Initialized empty Git repository in .git/
```

That last line means we’ve gone and done it–successfully created a Git directory for our project; however, note that it’s an _empty_ Git repo. To add all the files and folders in your project’s directory you’ll first need to take a “snapshot” of all the contents in project/ and don’t forget to include the ‘ .’

```bash
$ git add .
```

This command, despite being called add doesn’t add the contents of the working directory into Git; what it does is store the snapshot in the “index”–a temporary staging area to keep track of your changes without changing the Git repository itself. To permanently store the contents of your working directory use:

```bash
$ git commit
```

This will pull up a commit message in your preferred text editor:

```bash
 # Please enter the commit message for your changes. Lines starting
 # with '#' will be ignored, and an empty message aborts the commit.
 #
 # On branch master
 #
 # Initial commit
 #
 # Changes to be committed:
 #    new file:   closing.txt
 #    new file:   file.txt
 #
```

A commit message has two components, the _subject_ and, separated by a blank line, the _body_. The subject should be a short message summarizing the changes made by the commit, with a more thorough description of what changed in the body. In this case, we introduced our files to the Git directory for the first time, otherwise known as our initial commit. It seems to me that we could simply uncomment lines 7-12 for a perfectly good commit message.

```bash
Initial commit

Changes to be commited:
    new file:   closing.txt
    new file:   file.txt
```

Note the blank lines after the subject _and_ after the last line of the body. The former is required, but while the latter is not, it’s considered good form to leave a blank line after the message. In any case, you’re done! You’ve successfully added your project files to Git.
