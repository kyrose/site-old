---
layout: page
title: Blog
description: "List of blog posts by Kyanne Rose"
permalink: "blog/"
image: assets/images/img04.jpg
---

{% for post in site.posts %}

<section class="blog">
  <a href="{{ post.url | relative_url }}" class="link">
    <div class="content">
      <small>{{ post.timestamp }}</small>
      <h2>{{ post.title }}</h2>
      <p>{{ post.excerpt }}</p>
    </div>
  </a>
</section>
<br>
{% endfor %}
