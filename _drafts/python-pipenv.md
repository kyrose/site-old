# Using `pip` and `pipenv` with Python

## The `pip` module

Included by default since Python 3.4

(From [pip.pypa.io](https://pip.pypa.io/en/latest/installing/))

#### With get-pip.py:

``` bash
ine 9, in <module>
    from pip import main
ImportError: cannot import name 'main'

```

get-pip.py also installs `setuptools` and `wheel`. 

##### What is the error message telling us?

There is no module named ‘==main==’. How does pip find modules? How does it cache them?

#### With Ubuntu:

```bash
sudo apt install python-pip #Python 2
sudo apt install python3-venv python3-pip #Python3
```



### User Installs

#### `site.USER_BASE`

Path to base directory for the user site-packages. ==Default value is ~/.local==. Used by Distutils to find installation directories for Python modules for the user installation scheme.

See also: [PYTHONUSERBASE](https://docs.python.org/3/using/cmdline.html#envvar-PYTHONUSERBASE) 