---
layout: page
title: Tags
description: An archive of posts sorted by tag.
image: assets/images/img03.jpg
permalink: /tags.html
---

{% for tag in site.tags %}

  <h3>{{ tag[0] }}</h3>
  <ul>
    {% for post in tag[1] %}
      <li><a href="{{ post.url }}">{{ post.title }}</a></li>
    {% endfor %}
  </ul>
{% endfor %}
