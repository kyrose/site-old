---
layout: home
title: {{ site.title }}
description: {{ site.description }}
aboutme: "I'm a former physicist who now spends her days in Los Angeles learning Python and reading cryptography papers for fun. I've made this site to showcase projects I create; and a blog to track my progress and thought process during a project's development, to summarize what I learn as I research, and to chronicle my growth as a programmer."
---
